module.exports = function(app){
    var accounts = require('./controllers/accounts');
	
	//List All
    app.get('/accounts', accounts.findAll);
    
	//Login
	app.post('/login', accounts.findById);
    
	//Register
	app.post('/register', accounts.add);
	
    // app.put('/accounts/:id', accounts.update);
    // app.delete('/accounts/:id', accounts.delete);
	
	app.get('/import', accounts.import);
}