var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var AccountSchema = new Schema({
  fullname: String,
  username: String,
  password: String,
  email: String
});

mongoose.model('Account', AccountSchema);