var mongoose = require('mongoose'),
Account = mongoose.model('Account');

exports.findAll = function(req, res){
  Account.find({},function(err, results) {
    return res.send(results);
  });
};

exports.import = function(req, res){
  Account.create(
    { "fullname": "DJ Code Red", "username": "Ben", "password": "Reason", "email":"Ben@mail.com" },
    { "fullname": "Kingston Kats", "username": "Mike","password": "drums", "email":"Mike@mail.com"  },
    { "fullname": "The Eyeliner", "username": "Paul", "password": "guitar" , "email":"Paul@mail.com" }
  , function (err) {
    if (err) return console.log(err);
    return res.send(202);
  });
};
 
exports.findById = function(req, res){
  var username = req.body.username;
  var password = req.body.password; 
  
  Account.findOne({'username': username,'password': password },function(err, result) {
	  if(result){
		result = ({"Message":"Berhasil Login"});
	  }else{
		result = ({"Message":"Gagal Login"});  
	  } 
	return res.send(result);
  });
};

exports.add = function(req, res) {
  Account.create(req.body, function (err, result) {
    if (err) return console.log(err);
    return res.send({"Message":"Berhasil Sign Up"});
  });
}

exports.update = function(req, res) {
  var id = req.params.id;
  var updates = req.body;

  Account.update({"_id":id}, req.body,
    function (err, numberAffected) {
      if (err) return console.log(err);
      console.log('Updated %d musicians', numberAffected);
      res.send(202);
  });
}

exports.delete = function(req, res){
  var id = req.params.id;
  Account.remove({'_id':id},function(result) {
    return res.send(result);
  });
};



