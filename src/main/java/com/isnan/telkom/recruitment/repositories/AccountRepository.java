/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isnan.telkom.recruitment.repositories;

/**
 *
 * @author Yasmin
 */

import com.isnan.telkom.recruitment.models.Account;
import org.springframework.data.repository.CrudRepository;


public interface AccountRepository extends CrudRepository<Account, String> {
    @Override
    Account findOne(String id);

    @Override
    void delete(Account deleted);
}
