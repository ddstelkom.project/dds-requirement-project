/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isnan.telkom.recruitment.controllers;

import com.isnan.telkom.recruitment.models.Account;
import com.isnan.telkom.recruitment.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Yasmin
 */
@RestController
public class AccountController {
    
    @Autowired
    AccountRepository accountRepository;

    
    
    @RequestMapping(value="/halo", method = RequestMethod.GET)
    public String showHaloPage(ModelMap model){
        return "halo";
    }
    
    @RequestMapping(method=RequestMethod.GET, value="/account")
    public Iterable<Account> account() {
        return accountRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/account")
    public String save(@RequestBody Account account) {
        accountRepository.save(account);

        return account.getId();
    }

    @RequestMapping(method=RequestMethod.GET, value="/account/{id}")
    public Account show(@PathVariable String id) {
        return accountRepository.findOne(id);
    }

    @RequestMapping(method=RequestMethod.PUT, value="/account/{id}")
    public Account update(@PathVariable String id, @RequestBody Account account) {
        Account acc = accountRepository.findOne(id);
        if(account.getFullName() != null)
            acc.setFullName(acc.getFullName());
        if(account.getUserName() != null)
            acc.setUserName(acc.getUserName());
        if(account.getPassword() != null)
            acc.setPassword(acc.getPassword());
        if(account.getEmail() != null)
            acc.setEmail(acc.getEmail());
        if(account.getNamaFile() != null)
            acc.setNamaFile(acc.getNamaFile());
        if(account.getLokasiFile() != null)
            acc.setLokasiFile(acc.getLokasiFile());
        if(account.getJurusan() != null)
            acc.setJurusan(acc.getJurusan());
        
        accountRepository.save(acc);
        return acc;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/account/{id}")
    public String delete(@PathVariable String id) {
        Account account = accountRepository.findOne(id);
        accountRepository.delete(account);

        return "account has been deleted";
    }
}
